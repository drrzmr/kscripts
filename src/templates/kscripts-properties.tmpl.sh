#!/usr/bin/env -S bash -euo pipefail

source ${KSCRIPTS_ROOT}/src/helpers/filename.sh

properties=$(helper_filename_properties ${cluster} ${service_account})

case ${security} in

PLAIN) cat > ${properties} << PROPERTIES
security.protocol=SASL_SSL
sasl.mechanism=PLAIN
sasl.jaas.config=org.apache.kafka.common.security.plain.PlainLoginModule required username="${broker_username}" password="${broker_password}";
PROPERTIES
;;

SCRAM-SHA-256) cat > ${properties} << PROPERTIES
security.protocol=SASL_SSL
sasl.mechanism=SCRAM-SHA-256
sasl.jaas.config=org.apache.kafka.common.security.scram.ScramLoginModule required username="${broker_username}" password="${broker_password}";
PROPERTIES
;;

NONE) cat > ${properties} << PROPERTIES
security.protocol=PLAINTEXT
PROPERTIES
;;

esac

echo ${properties}
unset properties
