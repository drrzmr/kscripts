#!/usr/bin/env -S bash -euo pipefail

source ${KSCRIPTS_ROOT}/src/helpers/filename.sh

wrapper=$(helper_filename_wrapper ${cluster} ${service_account} 'acls-list-principals')

cat > ${wrapper} << TEMPLATE
#!/usr/bin/env -S bash -euo pipefail
debug=\${DEBUG:-"false"}
[[ \${debug} != "false" ]] && set -x
exec kafka-acls \\
  --bootstrap-server ${bootstrap_server} \\
  --command-config ${properties} \\
  --list | grep 'principal=' | awk -F'[=,]' '{print \$2}' | sort | uniq
TEMPLATE

echo ${wrapper}
unset wrapper
