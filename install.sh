#!/usr/bin/env -S bash -euo pipefail

KSCRIPTS_ROOT=${KSCRIPTS_ROOT:-${HOME}/.kscripts}
KSCRIPTS_PROPERTIES=${KSCRIPTS_PROPERTIES:-/keybase/private/$(whoami)/kscripts/properties}
KSCRIPTS_REPO=https://github.com/drrzmr/kscripts.git

echo ' _                  _       _'
echo '| | _____  ___ _ __(_)_ __ | |_ ___'
echo '| |/ / __|/ __|  __| |  _ \| __/ __|'
echo '|   <\__ \ (__| |  | | |_) | |_\__ \'
echo '|_|\_\___/\___|_|  |_|  __/ \__|___/'
echo '                     |_|'

errors=0
if [[ -d ${KSCRIPTS_ROOT} ]]; then
	echo "> kscripts already instaled on: ${KSCRIPTS_ROOT}"
	errors=$((errors+1))
fi

if [[ -d ${KSCRIPTS_PROPERTIES} ]]; then
	echo "> kscripts properties already exists on: ${KSCRIPTS_PROPERTIES}"
	errors=$((errors+1))
fi
[[ ${errors} -gt 0 ]] && exit ${errors}

mkdir -p ${KSCRIPTS_ROOT}/bin
mkdir -p ${KSCRIPTS_ROOT}/src/helpers
mkdir -p ${KSCRIPTS_ROOT}/src/commands
mkdir -p ${KSCRIPTS_ROOT}/src/templates
mkdir -p ${KSCRIPTS_PROPERTIES}

git clone ${KSCRIPTS_REPO} ${KSCRIPTS_ROOT}/repo

cp ${KSCRIPTS_ROOT}/repo/bin/kscripts.sh ${KSCRIPTS_ROOT}/bin
cp ${KSCRIPTS_ROOT}/repo/src/helpers/*.sh ${KSCRIPTS_ROOT}/src/helpers
cp ${KSCRIPTS_ROOT}/repo/src/commands/kscripts-*-command.sh ${KSCRIPTS_ROOT}/src/commands
cp ${KSCRIPTS_ROOT}/repo/src/templates/kscripts-*.tmpl.sh ${KSCRIPTS_ROOT}/src/templates
